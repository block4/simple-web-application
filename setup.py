#!/usr/bin/env python3
from setuptools import setup
from setuptools import find_packages

setup(
    name='simple-web-application',
    version='0.0.1',
    scripts=['SimpleWebApplication.py'],
    packages=find_packages(),
    url='https://gitlab.com/block4/simple-web-application',
    license='Mozilla Public License Version 2.0',
    author='block4 GitLab Group',
    author_email='',
    description='Simple Web Application'
)
