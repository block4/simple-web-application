curl --fail -XPOST -H "Content-type: application/json" -d '{"name":"'$CI_COMMIT_REF_NAME'"}' 'https://gitlabblock5:'$FEATURE_MANAGER_TOKEN'@'$FEATURE_MANAGER_DOMAIN'/lxd/container'
echo ""
tar cvfz $CI_PIPELINE_ID.tar.gz dist/
echo ""
curl --fail -F "artifact=@$CI_PIPELINE_ID.tar.gz" -F "container_name=$CI_COMMIT_REF_NAME" "https://gitlabblock5:"$FEATURE_MANAGER_TOKEN"@"$FEATURE_MANAGER_DOMAIN"/lxd/container/upload"
echo ""