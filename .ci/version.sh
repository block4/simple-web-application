if [ -n "$CI_BUILD_TAG" ]
then
  sed -i "s/0.0.1/$CI_BUILD_TAG/g" $CI_PROJECT_DIR/setup.py
else
  sed -i "s/0.0.1/$CI_PIPELINE_ID/g" $CI_PROJECT_DIR/setup.py
fi

sed -i "s#PROJECT_NAME#$CI_PROJECT_NAME#g" $CI_PROJECT_DIR/setup.py
sed -i "s#PROJECT_URL#$CI_PROJECT_URL#g" $CI_PROJECT_DIR/setup.py
